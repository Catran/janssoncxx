#ifndef __ARRAY_H__
#define __ARRAY_H__

#include "helper.h"
#include "object.h"

#include <jansson.h>
#include <string>

namespace janssoncxx
{
	class Object;

	class Array
	{
	public:
		/* dump */
		/* remove after debug */
		std::string dump() const
		{
			char* dump = json_dumps(root_, JSON_ENCODE_ANY);
			if (dump == NULL) {
				return "";
			}

			std::string dump_s = std::string(dump);
			free(dump);
			return dump_s;
		}

		Array();
		Array(const json_t* root);
		Array(const Array& object);
		~Array();
		Array& operator=(const Array& object);

		friend bool operator==(const Array& lhs, const Array& rhs);
		friend bool operator!=(const Array& lhs, const Array& rhs);

		/* append */
		void append(const int value);
		void append(const double value);
		void append(const std::string& value);
		void append(const char* value);
		void append(const bool value);
		void append(const Array& value);
		void append(const Object& value);
		void append();

		/* insert */
		void insert(const int index, const int value);
		void insert(const int index, const double value);
		void insert(const int index, const std::string& value);
		void insert(const int index, const char* value);
		void insert(const int index, const bool value);
		void insert(const int index, const Array& value);
		void insert(const int index, const Object& value);
		void insert(const int index);

		/* get */
		//template<class T>
		//T get(const int index) const;
		//template<>
		//int get<int>(const int index) const;
		//template<>
		//double get<double>(const int index) const;
		//template<>
		//std::string get<std::string>(const int index) const;
		//template<>
		//bool get<bool>(const int index) const;
		//template<>
		//Array get<Array>(const int index) const;
		//template<>
		//Object get<Object>(const int index) const;

		/* get with default value */
		int get(const int index, const int value) const;
		double get(const int index, const double value) const;
		std::string get(const int index, const char* value) const;
		std::string get(const int index, const std::string& value) const;
		bool get(const int index, const bool value) const;
		Object get(const int index, const Object& value) const;
		Array get(const int index, const Array& value) const;

		/* has */
		bool has(const int index, const Type type = Type::JSON_ANY) const;

		/* remove */
		void remove(const int index);

		/* raw */
		json_t* getRawVlaue() const;

		/* clear */
		void clear();

		/* size */
		size_t size() const;

	private:
		json_t* root_;
	};
	
	//template<class T>
	//inline T Array::get(const int index) const
	//{
	//	return T();
	//}

	//template<>
	//inline int Array::get<int>(const int index) const
	//{
	//	const json_t* object = json_array_get(root_, index);
	//	return json_integer_value(object);
	//}

	//template<>
	//inline double Array::get<double>(const int index) const
	//{
	//	const json_t* object = json_array_get(root_, index);
	//	return json_real_value(object);
	//}

	//template<>
	//inline std::string Array::get<std::string>(const int index) const
	//{
	//	const json_t* object = json_array_get(root_, index);
	//	return json_string_value(object);
	//}

	//template<>
	//inline bool Array::get<bool>(const int index) const
	//{
	//	const json_t* object = json_array_get(root_, index);
	//	return json_boolean_value(object);
	//}

	//template<>
	//inline Array Array::get<Array>(const int index) const
	//{
	//	const json_t* object = json_array_get(root_, index);
	//	return Array(object);
	//}
}

#endif /* __ARRAY_H__ */