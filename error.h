#ifndef __ERROR_H__
#define __ERROR_H__

#include <jansson.h>
#include <string>

namespace janssoncxx
{
	class Error
	{
	public:
		Error();
		Error(json_error_t error);

		int getLine() const;
		int getColumn() const;
		int getPosition() const;
		std::string getSource() const;
		std::string getText() const;

		bool hasErrors() const;

	private:
		json_error_t error_;
		bool hasErrors_;
	};
}

#endif /* __ERROR_H__ */