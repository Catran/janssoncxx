#include "array.h"

//----------------------------------------------------------------------
// INIT BLOCK
//----------------------------------------------------------------------
janssoncxx::Array::Array()
{
	root_ = json_array();
}

janssoncxx::Array::Array(const json_t* root)
{
	if (json_is_array(root))
		root_ = json_deep_copy(root);
	else
		root_ = json_object();
}

janssoncxx::Array::Array(const Array& object)
{
	if (json_is_array(object.root_))
		root_ = json_deep_copy(object.root_);
	else
		root_ = json_object();
}

janssoncxx::Array::~Array()
{
	json_decref(root_);
}

janssoncxx::Array& janssoncxx::Array::operator=(const Array& object)
{
	if (json_is_array(object.root_))
		root_ = json_deep_copy(object.root_);
	else
		root_ = json_object();

	return *this;
}

bool janssoncxx::operator==(const Array& lhs, const Array& rhs)
{
	return json_equal(lhs.root_, rhs.root_) != NULL;
}

bool janssoncxx::operator!=(const Array& lhs, const Array& rhs)
{
	return !(lhs == rhs);
}

//----------------------------------------------------------------------
// APPEND BLOCK
//----------------------------------------------------------------------
void janssoncxx::Array::append(const int value)
{
	json_array_append_new(root_, json_integer(value));
}

void janssoncxx::Array::append(const double value)
{
	json_array_append_new(root_, json_real(value));
}
void janssoncxx::Array::append(const std::string& value)
{
	append(value.c_str());
}
void janssoncxx::Array::append(const char* value)
{
	json_array_append_new(root_, json_string(value));
}
void janssoncxx::Array::append(const bool value)
{
	json_array_append_new(root_, json_boolean(value));
}
void janssoncxx::Array::append(const Array& value)
{
	if (*this != value)
	{
		json_t* res = json_deep_copy(value.getRawVlaue());
		if (json_is_array(res)) {
			json_array_append_new(root_, res);
		}
	}
}

void janssoncxx::Array::append(const Object& value)
{
	json_t* res = json_deep_copy(value.getRawVlaue());
	if (json_is_object(res)) {
		json_array_append_new(root_, res);
	}
}

void janssoncxx::Array::append()
{
	json_array_append_new(root_, json_null());
}

//----------------------------------------------------------------------
// INSERT BLOCK
//----------------------------------------------------------------------
void janssoncxx::Array::insert(const int index, const int value)
{
	json_array_insert_new(root_, index, json_integer(value));
}
void janssoncxx::Array::insert(const int index, const double value)
{
	json_array_insert_new(root_, index, json_real(value));
}
void janssoncxx::Array::insert(const int index, const std::string& value)
{
	insert(index, value.c_str());
}
void janssoncxx::Array::insert(const int index, const char* value)
{
	json_array_insert_new(root_, index, json_string(value));
}
void janssoncxx::Array::insert(const int index, const bool value)
{
	json_array_insert_new(root_, index, json_boolean(value));
}
void janssoncxx::Array::insert(const int index, const Array& value)
{
	if (*this != value)
		json_array_insert_new(root_, index, value.getRawVlaue());
}

void janssoncxx::Array::insert(const int index, const Object& value)
{
	json_t* res = json_deep_copy(value.getRawVlaue());
	if (json_is_object(res)) {
		json_array_append_new(root_, res);
	}
}

void janssoncxx::Array::insert(const int index)
{
	json_array_insert_new(root_, index, json_null());
}

//----------------------------------------------------------------------
// GET BLOCK
//----------------------------------------------------------------------
int janssoncxx::Array::get(const int index, const int value) const
{
	int res = value;
	const json_t* array = json_array_get(root_, index);
	if (json_is_integer(array)) {
		res = json_integer_value(array);
	}
	return res;
}

double janssoncxx::Array::get(const int index, const double value) const
{
	double res = value;
	const json_t* array = json_array_get(root_, index);
	if (json_is_real(array)) {
		res = json_real_value(array);
	}
	return res;
}

std::string janssoncxx::Array::get(const int index, const char* value) const
{
	std::string res = value;
	const json_t* array = json_array_get(root_, index);
	if (json_is_string(array)) {
		res = json_string_value(array);
	}
	return res;
}

std::string janssoncxx::Array::get(const int index, const std::string& value) const
{
	return get(index, value.c_str());
}

bool janssoncxx::Array::get(const int index, const bool value) const
{
	bool res = value;
	const json_t* array = json_array_get(root_, index);
	if (json_is_boolean(array)) {
		res = json_boolean_value(array);
	}
	return res;
}

janssoncxx::Object janssoncxx::Array::get(const int index, const Object& value) const
{
	Object res = value;
	const json_t* object = json_array_get(root_, index);
	if (json_is_object(object)) {
		res = Object(object);
	}
	return res;
}

janssoncxx::Array janssoncxx::Array::get(const int index, const Array& value) const
{
	Array res = value;
	const json_t* object = json_array_get(root_, index);
	if (json_is_array(object)) {
		res = Array(object);
	}
	return res;
}

//----------------------------------------------------------------------
// HELPER BLOCK
//----------------------------------------------------------------------
bool janssoncxx::Array::has(const int index, const Type type) const
{
	const json_t* res = json_array_get(root_, index);

	switch (type)
	{
	case Type::JSON_INTEGER: return json_is_integer(res);
	case Type::JSON_DOUBLE: return json_is_real(res);
	case Type::JSON_STRING: return json_is_string(res);
	case Type::JSON_BOOLEAN: return json_is_boolean(res);
	case Type::JSON_OBJECT: return json_is_object(res);
	case Type::JSON_ARRAY: return json_is_array(res);
	case Type::JSON_NULL: return json_is_null(res);
	case Type::JSON_ANY: return res != NULL;
	default: return false;
	}
}

void janssoncxx::Array::remove(const int index)
{
	json_array_remove(root_, index);
}

json_t* janssoncxx::Array::getRawVlaue() const
{
	return root_;
}

void janssoncxx::Array::clear()
{
	json_array_clear(root_);
}

size_t janssoncxx::Array::size() const
{
	return json_array_size(root_);
}
//----------------------------------------------------------------------
//----------------------------------------------------------------------