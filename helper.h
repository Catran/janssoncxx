#ifndef __HELPER_H__
#define __HELPER_H__

namespace janssoncxx
{
	enum class Type {
		JSON_INTEGER,
		JSON_DOUBLE,
		JSON_STRING,
		JSON_OBJECT,
		JSON_ARRAY,
		JSON_BOOLEAN,
		JSON_NULL,
		JSON_ANY,
	};
}

#endif /* __HELPER_H__ */