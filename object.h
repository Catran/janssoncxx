#ifndef __OBJECT_H__
#define __OBJECT_H__

#include "helper.h"
#include "array.h"
#include "error.h"

#include <jansson.h>
#include <string>
#include <vector>

namespace janssoncxx
{
	class Array;

	class Object
	{
	public:
		Object();
		Object(const json_t* root);
		Object(const Object& object);
		~Object();
		Object& operator=(const Object& object);

		friend bool operator==(const Object& lhs, const Object& rhs);
		friend bool operator!=(const Object& lhs, const Object& rhs);
		
		/* set */
		void set(const std::string& key, const int value);
		void set(const std::string& key, const double value);
		void set(const std::string& key, const std::string& value);
		void set(const std::string& key, const char* value);
		void set(const std::string& key, const bool value);
		void set(const std::string& key, const Object& value);
		void set(const std::string& key, const Array& value);
		void set(const std::string& key);

		/* get */
		//template<class T>
		//T get(const std::string& key) const;
		//template<>
		//int get<int>(const std::string& key) const;
		//template<>
		//double get<double>(const std::string& key) const;
		//template<>
		//std::string get<std::string>(const std::string& key) const;
		//template<>
		//bool get<bool>(const std::string& key) const;
		//template<>
		//Object get<Object>(const std::string& key) const;
		//template<>
		//Array get<Array>(const std::string& key) const;

		/* get with default value */
		int get(const std::string& key, const int value) const;
		double get(const std::string& key, const double value) const;
		std::string get(const std::string& key, const char* value) const;
		std::string get(const std::string& key, const std::string& value) const;
		bool get(const std::string& key, const bool value) const;
		Object get(const std::string& key, const Object& value) const;
		Array get(const std::string& key, const Array& value) const;

		/* has */
		bool has(const std::string& key, const Type type = Type::JSON_ANY) const;

		/* get type by key */
		Type getType(const std::string& key) const;

		/* remove */
		void remove(const std::string& key);

		/* parse */
		Error parse(const std::string& input);

		/* raw */
		json_t* getRawVlaue() const;

		/* dump */
		std::string dump() const;

		/* clear */
		void clear();

		/* size */
		size_t size() const;

		/* get all keys */
		std::vector<std::string> getAllKeys() const;

	private:
		json_error_t* error_;
		json_t* root_;
	};

	//----------------------------------------------------------------------
	// GET BLOCK
	//----------------------------------------------------------------------
	//template<class T>
	//inline T Object::get(const std::string& key) const
	//{
	//	return T();
	//}

	//template<>
	//inline int Object::get<int>(const std::string& key) const
	//{
	//	const json_t* object = json_object_get(root_, key.c_str());
	//	return json_integer_value(object);
	//}

	//template<>
	//inline double Object::get<double>(const std::string& key) const
	//{
	//	const json_t* object = json_object_get(root_, key.c_str());
	//	return json_real_value(object);
	//}

	//template<>
	//inline std::string Object::get<std::string>(const std::string& key) const
	//{
	//	const json_t* object = json_object_get(root_, key.c_str());
	//	return json_string_value(object);
	//}

	//template<>
	//inline bool Object::get<bool>(const std::string& key) const
	//{
	//	const json_t* object = json_object_get(root_, key.c_str());
	//	return json_boolean_value(object);
	//}

	//template<>
	//inline Object Object::get<Object>(const std::string& key) const
	//{
	//	const json_t* object = json_object_get(root_, key.c_str());
	//	return Object(object);
	//}
}
#endif /* __OBJECT_H__ */