#include "error.h"

janssoncxx::Error::Error() : hasErrors_(false)
{
}

janssoncxx::Error::Error(json_error_t error): error_(error), hasErrors_(true)
{
}

int janssoncxx::Error::getLine() const
{
	return error_.line;
}

int janssoncxx::Error::getColumn() const
{
	return error_.column;
}

int janssoncxx::Error::getPosition() const
{
	return error_.position;
}

std::string janssoncxx::Error::getSource() const
{
	return std::string(error_.source);
}

std::string janssoncxx::Error::getText() const
{
	return std::string(error_.text);
}

bool janssoncxx::Error::hasErrors() const
{
	return hasErrors_;
}
