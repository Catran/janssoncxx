#include "object.h"

//----------------------------------------------------------------------
// INIT BLOCK
//----------------------------------------------------------------------
janssoncxx::Object::Object()
{
	root_ = json_object();
}

janssoncxx::Object::Object(const json_t* root)
{
	if (json_is_object(root))
		root_ = json_deep_copy(root);
	else
		root_ = json_object();
}

janssoncxx::Object::Object(const Object& object)
{
	if (json_is_object(object.root_))
		root_ = json_deep_copy(object.root_);
	else
		root_ = json_object();
}

janssoncxx::Object::~Object()
{
	json_decref(root_);
}

janssoncxx::Object& janssoncxx::Object::operator=(const Object& object)
{
	if (json_is_object(object.root_))
		root_ = json_deep_copy(object.root_);
	else
		root_ = json_object();

	return *this;
}

bool janssoncxx::operator==(const Object& lhs, const Object& rhs)
{
	return json_equal(lhs.root_, rhs.root_) != NULL;
}

bool janssoncxx::operator!=(const Object& lhs, const Object& rhs)
{
	return !(lhs == rhs);
}

//----------------------------------------------------------------------
// SET BLOCK
//----------------------------------------------------------------------
void janssoncxx::Object::set(const std::string& key, const int value)
{
	json_object_set_new(root_, key.c_str(), json_integer(value));
}

void janssoncxx::Object::set(const std::string& key, const double value)
{
	json_object_set_new(root_, key.c_str(), json_real(value));
}

void janssoncxx::Object::set(const std::string& key, const std::string& value)
{
	set(key, value.c_str());
}

void janssoncxx::Object::set(const std::string& key, const char* value)
{
	json_object_set_new(root_, key.c_str(), json_string(value));
}

void janssoncxx::Object::set(const std::string& key, const bool value)
{
	json_object_set_new(root_, key.c_str(), json_boolean(value));
}

void janssoncxx::Object::set(const std::string& key, const Object& value)
{

	if (*this != value)
	{
		json_t* res = json_deep_copy(value.getRawVlaue());
		if (json_is_object(res)) {
			json_object_set_new(root_, key.c_str(), res);	
		}
	}
}

void janssoncxx::Object::set(const std::string& key, const Array& value)
{
	json_t* res = json_deep_copy(value.getRawVlaue());
	if (json_is_array(res)) {
		json_object_set_new(root_, key.c_str(), res);
	}
}

void janssoncxx::Object::set(const std::string& key)
{
	json_object_set_new(root_, key.c_str(), json_null());
}

//----------------------------------------------------------------------
// GET BLOCK
//----------------------------------------------------------------------
int janssoncxx::Object::get(const std::string& key, const int value) const
{
	int res = value;
	const json_t* object = json_object_get(root_, key.c_str());
	if (json_is_integer(object)) {
		res = json_integer_value(object);
	}
	return res;
}

double janssoncxx::Object::get(const std::string& key, const double value) const
{
	double res = value;
	const json_t* object = json_object_get(root_, key.c_str());
	if (json_is_real(object)) {
		res = json_real_value(object);
	}
	return res;
}
std::string janssoncxx::Object::get(const std::string& key, const char* value) const
{
	std::string res = value;
	const json_t* object = json_object_get(root_, key.c_str());
	if (json_is_string(object)) {
		res = json_string_value(object);
	}
	return res;
}

std::string janssoncxx::Object::get(const std::string& key, const std::string& value) const
{
	return get(key, value.c_str());
}

bool janssoncxx::Object::get(const std::string& key, const bool value) const
{
	bool res = value;
	const json_t* object = json_object_get(root_, key.c_str());
	if (json_is_boolean(object)) {
		res = json_boolean_value(object);
	}
	return res;
}

janssoncxx::Object janssoncxx::Object::get(const std::string& key, const Object& value) const
{
	Object res = value;
	const json_t* object = json_object_get(root_, key.c_str());
	if (json_is_object(object)) {
		res = Object(object);
	}
	return res;
}

janssoncxx::Array janssoncxx::Object::get(const std::string& key, const Array& value) const
{
	Array res = value;
	const json_t* object = json_object_get(root_, key.c_str());
	if (json_is_array(object)) {
		res = Array(object);
	}
	return res;
}

//----------------------------------------------------------------------
// HELPER BLOCK
//----------------------------------------------------------------------
bool janssoncxx::Object::has(const std::string& key, const Type type) const
{
	const json_t* res = json_object_get(root_, key.c_str());

	switch (type)
	{
	case Type::JSON_INTEGER: return json_is_integer(res);
	case Type::JSON_DOUBLE: return json_is_real(res);
	case Type::JSON_STRING: return json_is_string(res);
	case Type::JSON_BOOLEAN: return json_is_boolean(res);
	case Type::JSON_OBJECT: return json_is_object(res);
	case Type::JSON_ARRAY: return json_is_array(res);
	case Type::JSON_NULL: return json_is_null(res);
	case Type::JSON_ANY: return res != NULL;
	default: return false;
	}
}

janssoncxx::Type janssoncxx::Object::getType(const std::string& key) const
{
	const json_t* res = json_object_get(root_, key.c_str());

	switch (res->type)
	{
	case json_type::JSON_INTEGER: return Type::JSON_INTEGER;
	case json_type::JSON_REAL: return Type::JSON_DOUBLE;
	case json_type::JSON_STRING: return Type::JSON_STRING;
	case json_type::JSON_FALSE: return Type::JSON_BOOLEAN;
	case json_type::JSON_TRUE: return Type::JSON_BOOLEAN;
	case json_type::JSON_OBJECT: return Type::JSON_OBJECT;
	case json_type::JSON_ARRAY: return Type::JSON_ARRAY;
	case json_type::JSON_NULL: return Type::JSON_NULL;
	}
}

void janssoncxx::Object::remove(const std::string& key)
{
	json_object_del(root_, key.c_str());
}

janssoncxx::Error janssoncxx::Object::parse(const std::string& input)
{
	clear();
	json_error_t error;
	root_ = json_loads(input.c_str(), NULL, &error);
	if (root_ == NULL)
	{
		return Error(error);
	}
	return Error();
}

json_t* janssoncxx::Object::getRawVlaue() const
{
	return root_;
}

std::string janssoncxx::Object::dump() const
{
	char* dump = json_dumps(root_, JSON_COMPACT);
	if (dump == NULL) {
		return "";
	}

	std::string dump_s = std::string(dump);
	free(dump);
	return dump_s;
}

void janssoncxx::Object::clear()
{
	json_object_clear(root_);
}

size_t janssoncxx::Object::size() const
{
	return json_object_size(root_);
}

std::vector<std::string> janssoncxx::Object::getAllKeys() const
{
	std::vector<std::string> keys;
	const char* key;

	void* it = json_object_iter(root_);
	while (it != NULL) {
		key = json_object_iter_key(it);
		if (key != NULL) {
			keys.push_back(key);
		}
		it = json_object_iter_next(root_, it);
	}

	return keys;
}
//----------------------------------------------------------------------
//----------------------------------------------------------------------
